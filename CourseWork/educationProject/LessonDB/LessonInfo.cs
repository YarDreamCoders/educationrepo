﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using LessonLibrary;

namespace LessonDB
{
    public class LessonInfo
    {
        public string Title;
        public string Autor;
        public DateTime DateCreate;
    }
}
