﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Windows;
using System.Windows.Media.Imaging;
using Image = System.Windows.Controls.Image;

namespace LessonLibrary
{
    [Serializable]
    public class ImageInfo : IData
    {
        [NonSerialized] 
        private readonly Image _imageControl;
        public int ImageHeight;
        public int ImageWidth;

        public byte[] Bytes { get; private set; }

        public ImageInfo(string pathToImage)
        {
            try
            {
                _imageControl = new Image {Source = new BitmapImage(new Uri(pathToImage))};
                var imageDrawing = System.Drawing.Image.FromFile(pathToImage);

                var memoryStream = new MemoryStream();
                imageDrawing.Save(memoryStream, GetImageFormatByFileExtension(pathToImage));
                Bytes = memoryStream.ToArray();

                //save value for serialization
                var file = System.Drawing.Image.FromFile(pathToImage);
                ImageHeight = file.Height;
                ImageWidth = file.Width;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        public ImageInfo(byte[] bytes, int height, int width)
        {
            Bytes = bytes;
            var memoryStream = new MemoryStream(bytes);
            System.Drawing.Image imageSource = System.Drawing.Image.FromStream(memoryStream);

            var bitmap = new Bitmap(imageSource);
            using (var stream = new MemoryStream())
            {
                bitmap.Save(stream, ImageFormat.Jpeg);

                stream.Position = 0;
                var result = new BitmapImage();
                result.BeginInit();
                result.CacheOption = BitmapCacheOption.OnLoad;
                result.StreamSource = stream;
                result.EndInit();
                result.Freeze();

                _imageControl = new Image
                {
                    Source = result,
                    Height = height,
                    Width = width,
                    HorizontalAlignment = HorizontalAlignment.Center
                };

                //save for serialization
                ImageHeight = height;
                ImageWidth = width;
            }
        }

        private ImageFormat GetImageFormatByFileExtension(string pathToFile)
        {
            var fileExtension = Path.GetExtension(pathToFile);
            if (fileExtension == null) throw new ArgumentException();

            switch (fileExtension.ToUpper())
            {
                case ".JPEG":
                case ".JPG" :
                    return ImageFormat.Jpeg;
                case ".BMP":
                    return ImageFormat.Bmp;
                case ".PNG":
                    return ImageFormat.Png;
                default:
                    throw new ArgumentException();
            }
        }

        public Image GetImageControl()
        {
            var imageInfo = new ImageInfo(Bytes, ImageHeight, ImageWidth);
            return imageInfo._imageControl;
        }
    }
}
