﻿namespace LessonLibrary
{
    public interface IGettingData
    {
        IData GetData();
    }
}
