﻿using System;
using System.Drawing;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Brushes = System.Windows.Media.Brushes;
using Image = System.Windows.Controls.Image;

namespace educationProject.Simple_classes
{
    class FolderPreview : StackPanel
    {
        public Image IconPreview { get; set; }

        public Label TextPreview { get; set; }

        public string FullName { get; set; }

        public FolderPreview(string sectionName)
        {
            FullName = sectionName;
            var bitmap = PictureResource.FolderIcon;

            var bitmapSource = Imaging.CreateBitmapSourceFromHBitmap(
                bitmap.GetHbitmap(),
                IntPtr.Zero,
                Int32Rect.Empty,
                BitmapSizeOptions.FromEmptyOptions());

            var cuttedFolderName = sectionName;
            const int maxLenghtFolderName = 13;
            if (cuttedFolderName.Length > maxLenghtFolderName)
                cuttedFolderName = cuttedFolderName.Substring(0, maxLenghtFolderName - 1) + "..";

            TextPreview = new Label { Content = cuttedFolderName, ToolTip = sectionName, HorizontalAlignment = HorizontalAlignment.Center, Cursor = Cursors.Hand, Foreground = Brushes.Azure, FontWeight = FontWeights.Bold };
            IconPreview = new Image { Source = bitmapSource, Width = 60, Height = 60, Cursor = Cursors.Hand };
            Children.Add(TextPreview);
            Children.Add(IconPreview);

            Background = GetComplexVisualBrush(PictureResource.BackgroundLessonPreview);
            Width = 120;
        }

        private VisualBrush GetComplexVisualBrush(Bitmap bitmap)
        {
            var myBrush = new VisualBrush();
            var bitmapSource = Imaging.CreateBitmapSourceFromHBitmap(
                bitmap.GetHbitmap(),
                IntPtr.Zero,
                Int32Rect.Empty,
                BitmapSizeOptions.FromEmptyOptions());
            var visualBrushFromImage = new Image() { Source = bitmapSource };
            myBrush.Visual = visualBrushFromImage;
            return myBrush;
        }
    }
}
