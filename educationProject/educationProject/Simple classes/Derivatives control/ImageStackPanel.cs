﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using LessonLibrary;

namespace educationProject
{
    public class ImageStackPanel : BaseStackPanel, IGettingData
    {
        private readonly byte[] _bytes;
        private readonly int _imageHeight;
        private readonly int _imageWidth;
        public ImageStackPanel(string pathToImage, StackPanel placeDeleting, bool userView)
        {
            try
            {
                var file = System.Drawing.Image.FromFile(pathToImage);
                _bytes = new ImageInfo(pathToImage).Bytes;
                IsModifiable = !userView;

                var uri = new Uri(pathToImage);
                var bitmap = new BitmapImage(uri);
                var image = new Image {Source = bitmap};

                if (file.Width > placeDeleting.ActualWidth)
                {
                    image.Width = placeDeleting.ActualWidth;
                    image.Height = file.Height * placeDeleting.ActualWidth / file.Width;
                }
                else
                {
                    image.Width = file.Width;
                    image.Height = file.Height;
                }

                //save size for transmitting data to ImageInfo for size-serialization!!
                _imageHeight = (int)image.Height;
                _imageWidth = (int)image.Width;

                if (IsModifiable)
                {
                    Children.Add(FactoryGrid.GetDeleteButton(this, placeDeleting));
                }
                Children.Add(image);
                InitializeImage();
            }
            catch
            {
            }

        }

        public ImageStackPanel(ImageInfo imageInfo, StackPanel placeDeleting, bool userView)
        {
            _bytes = imageInfo.Bytes;
            if (!userView)
            {
                Children.Add(FactoryGrid.GetDeleteButton(this, placeDeleting));
            }
            var image = imageInfo.GetImageControl();
            image.Width = imageInfo.ImageWidth;
            image.Height = imageInfo.ImageHeight;

            //FIX: for correct saving in editor
            _imageHeight = (int)image.Height;
            _imageWidth = (int)image.Width;
            Children.Add(image);
            InitializeImage();
        }

        private void InitializeImage()
        {
            Margin = new Thickness(10);
            DragEnter += ElementPanel_OnDragEnter;
            MouseDown += This_MouseDown;
            SetDiagonalLinesBackground();
        }

        private void SetDiagonalLinesBackground()
        {
            var bitmapSource = Imaging.CreateBitmapSourceFromHBitmap(PictureResource.WhiteDiagonalLines.GetHbitmap(),
                 IntPtr.Zero, Int32Rect.Empty, BitmapSizeOptions.FromEmptyOptions());
            Background = new ImageBrush(bitmapSource);
        }

        public IData GetData()
        {
            return new ImageInfo(_bytes, _imageHeight, _imageWidth);
        }
    }
}
