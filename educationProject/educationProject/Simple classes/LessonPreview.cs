﻿using System;
using System.Drawing;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Brushes = System.Windows.Media.Brushes;
using Image = System.Windows.Controls.Image;

namespace educationProject
{
    public class LessonPreview : StackPanel
    {

        public Image IconPreview { get; set; }

        public Label TextPreview { get; set; }

        public Image EditIcon { get; set; }

        public string FullNameWithoutExtension { get; set; }


        public LessonPreview(string lessonNameWithExtension)
        {
            FullNameWithoutExtension = lessonNameWithExtension.Split('.').First();
            var headerGrid = new Grid();
            EditIcon = new Image
            {
                Source = GetBitmapSource(PictureResource.EditIcon),
                Width = 20,
                Height = 20,
                HorizontalAlignment = HorizontalAlignment.Right,
                Cursor = Cursors.Hand,
                VerticalAlignment = VerticalAlignment.Top,
                ToolTip = "Редактировать Урок"
            };
            headerGrid.Children.Add(EditIcon);

            IconPreview = new Image { ToolTip = "Открыть урок", Source = GetBitmapSource(PictureResource.FileIcon), Width = 60, Height = 50, Cursor = Cursors.Hand };

            var cuttedLessonName = lessonNameWithExtension.Split('.').First();
            const int maxLenghtLessonName = 10;
            if (cuttedLessonName.Length > maxLenghtLessonName)
                cuttedLessonName = cuttedLessonName.Substring(0, maxLenghtLessonName - 1) + "..";
            TextPreview = new Label
            {
                Content = cuttedLessonName,
                ToolTip = lessonNameWithExtension.Split('.').First(),
                HorizontalAlignment = HorizontalAlignment.Center,
                Width = 80,
                Cursor = Cursors.Hand,
                Foreground = Brushes.Azure,
                FontWeight = FontWeights.Bold,
                HorizontalContentAlignment = HorizontalAlignment.Center
            };
            headerGrid.Children.Add(TextPreview);
            Children.Add(headerGrid);
            Children.Add(IconPreview);

            Width = 120;
            Background = GetComplexVisualBrush(PictureResource.BackgroundLessonPreview);
        }

        private VisualBrush GetComplexVisualBrush(Bitmap bitmap)
        {
            var myBrush = new VisualBrush();
            var bitmapSource = GetBitmapSource(bitmap);
            var visualBrushFromImage = new Image() { Source = bitmapSource };
            myBrush.Visual = visualBrushFromImage;
            return myBrush;
        }

        private BitmapSource GetBitmapSource(Bitmap resource)
        {
            var fileIconBitmapSource = Imaging.CreateBitmapSourceFromHBitmap(
                resource.GetHbitmap(),
                IntPtr.Zero,
                Int32Rect.Empty,
                BitmapSizeOptions.FromEmptyOptions());
            return fileIconBitmapSource;
        }
    }
}
