﻿using System;
using System.Windows;
using System.Windows.Controls;

namespace educationProject
{
    public static class FactoryTextBox
    {
        private static TextBox GetTextBox(string text, bool isModific, string toolTip = "")
        {
            var textBox = new TextBox
            {
                Margin = new Thickness(10),
                HorizontalContentAlignment = HorizontalAlignment.Stretch,
                HorizontalAlignment = HorizontalAlignment.Stretch,
                FontSize = 20,
                Text = text,
                ToolTip = toolTip,
                TextWrapping = new TextWrapping(),
                AcceptsReturn = true,
                IsEnabled = isModific
            };

            textBox.GotFocus += ToolTip_RemoveText;
            textBox.LostFocus += ToolTip_AddText;
            return textBox;
        }
        
        public static TextBox GetTextBoxAnswer(int number, bool isModific)
        {
            return GetTextBox("Ответ #" + (number + 1),isModific, "Ответ #" + (number + 1));
        }
        public static TextBox GetTextBoxDefault(string text, bool isModific, string toolTip="")
        {
            return GetTextBox(text, isModific, toolTip);
        }

        private static void ToolTip_RemoveText(object sender, EventArgs e)
        {
            var textBox = (TextBox)sender;
            if (textBox.Text == textBox.ToolTip.ToString())
            {
                textBox.Text = "";
            }
        }

        private static void ToolTip_AddText(object sender, EventArgs e)
        {
            var textBox = (TextBox)sender;
            if (textBox.Text == "")
            {
                textBox.Text = textBox.ToolTip.ToString();
            }
        }
       
    }
    
}
