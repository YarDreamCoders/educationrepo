﻿using System;
using System.Windows;
using LessonLibrary;

namespace educationProject
{
    public partial class MainWindow
    {
        private Lesson _lesson;

        private void GoToCreatingLessonButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (NameLessonTextBox.Text == (string)NameLessonTextBox.ToolTip)
                {
                    MessageBox.Show("Придумайте название урока", "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }

                if (PassworTextBox.Text == (string)PassworTextBox.ToolTip)
                {
                    MessageBox.Show("Придумайте пароль для редактирования урока", "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
                ElementPanel.Tag = false;
                InitializeLesson();
                ElementPanel.Children.Clear();
                MenuLesson.Visibility = Visibility.Hidden;    
                CreatingLesson.Visibility = Visibility.Visible;
                NameLessonForSaveButtonOK.Tag = "Create";
            }
            catch (Exception)
            {
                MessageBox.Show("Ваше название урока или пароль введены некорректно", "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void BackToStartMenu_OnClick(object sender, RoutedEventArgs e)
        {
            PasswordForEditLessonButtonEscape_Click(sender: null, e: null);

            if (PathsPanel.Visibility != Visibility.Hidden)
                ReturnPathCreateDirectories();
            RefreshDirectories();

            CreateSectionGrid.Visibility = Visibility.Hidden;
            TitleCreatingLesson.Visibility = Visibility.Hidden;

            MenuLesson.Visibility = Visibility.Visible;
            PathsPanel.Visibility = Visibility.Visible;
        }

        private void ReturnPathCreateDirectories()
        {
            //Убираем из пути последний каталог
            var pathArray = ViewWindowCreateSection.Tag.ToString().Split('/');
            Array.Resize(ref pathArray, pathArray.Length - 1);
            ViewWindowCreateSection.Tag = String.Join("/", pathArray);
            if (String.IsNullOrEmpty(ViewWindowCreateSection.Tag.ToString()))
                ViewWindowCreateSection.Tag = "DateBaseEducation";
        }

        private void InitializeLesson()
        {
            if (!string.IsNullOrEmpty(NameLessonTextBox.Text) && !string.IsNullOrEmpty(PassworTextBox.Text))
            {
                _lesson = new Lesson(NameLessonTextBox.Text, PassworTextBox.Text);
            }
            else
            {
                throw new NullReferenceException("Field password and title must not be empty");
            }
        }
    }
}
