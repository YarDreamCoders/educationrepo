﻿using System;
using System.IO;
using System.Windows;
using educationProject.Simple_classes;

namespace educationProject
{
    public partial class MainWindow
    {
        private void ViewWindowCreateSection_OnClick(object sender, RoutedEventArgs e)
        {
            PathsPanel.Visibility = Visibility.Hidden;
            TitleCreatingLesson.Visibility = Visibility.Hidden;
            CreateSectionGrid.Visibility = Visibility.Visible;
            PasswordForEditLessonButtonEscape_Click(sender: null, e: null);
        }
        private void CreateSection_OnClick(object sender, RoutedEventArgs e)
        {
            try
            {
                //Защита от placeHolder
                if (NameSection.Text == NameSection.ToolTip.ToString())
                {
                    MessageBox.Show("Введите название раздела!", "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }

                var sectionName = NameSection.Text;

                var path = ViewWindowCreateSection.Tag + "/" + NameSection.Text;
                if (!Directory.Exists(path))
                    Directory.CreateDirectory(path);
                else
                {
                    MessageBox.Show("Такой каталог уже существует!", "Ошибка", MessageBoxButton.OK,
                        MessageBoxImage.Error);
                    return;
                }

                var folderPreview = new FolderPreview(sectionName);
                folderPreview.MouseDown += SectionButtonClick;
                SectionPanel.Children.Add(folderPreview);

                NameSection.Text = "";

                CreateSectionGrid.Visibility = Visibility.Hidden;
                PathsPanel.Visibility = Visibility.Visible;

                RefreshDirectories();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void SectionButtonClick(object sender, RoutedEventArgs routedEventArgs)
        {
            if (PasswordForEditLessonGrid.Visibility == Visibility.Visible)
                return;
            MenuLesson.Visibility = Visibility.Visible;

            var folderPreview = (FolderPreview)sender;
            ViewWindowCreateSection.Tag += "/" + folderPreview.FullName;
            RefreshDirectories();
        }

        private void LogOutButton_OnClick(object sender, RoutedEventArgs e)
        {
            LogInMenu.Visibility = Visibility.Visible;
            MenuLesson.Visibility = Visibility.Hidden;
            PasswordForEditLessonButtonEscape_Click(sender: null, e: null);
        }
    }
}
